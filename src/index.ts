import * as express from 'express';
import { Request, Response } from 'express';

const app = express();
const port = 3005;

app.get("/", (req: Request, res: Response) => {
    res.send('Hello TypeScript')
});

const jsonResponse = '{"menu": {\n' +
    '    "header": "SVG Viewer",\n' +
    '    "items": [\n' +
    '        {"id": "Open"},\n' +
    '        {"id": "OpenNew", "label": "Open New"},\n' +
    '        null,\n' +
    '        {"id": "ZoomIn", "label": "Zoom In"},\n' +
    '        {"id": "ZoomOut", "label": "Zoom Out"},\n' +
    '        {"id": "OriginalView", "label": "Original View"},\n' +
    '        null,\n' +
    '        {"id": "Quality"},\n' +
    '        {"id": "Pause"},\n' +
    '        {"id": "Mute"},\n' +
    '        null,\n' +
    '        {"id": "Find", "label": "Find..."},\n' +
    '        {"id": "FindAgain", "label": "Find Again"},\n' +
    '        {"id": "Copy"},\n' +
    '        {"id": "CopyAgain", "label": "Copy Again"},\n' +
    '        {"id": "CopySVG", "label": "Copy SVG"},\n' +
    '        {"id": "ViewSVG", "label": "View SVG"},\n' +
    '        {"id": "ViewSource", "label": "View Source"},\n' +
    '        {"id": "SaveAs", "label": "Save As"},\n' +
    '        null,\n' +
    '        {"id": "Help"},\n' +
    '        {"id": "About", "label": "About Adobe CVG Viewer..."}\n' +
    '    ]\n' +
    '}}'
app.get("/api/v1", (req: Request, res: Response) => {
    res.json(JSON.parse(jsonResponse));
});
app.listen(port, () => {
    console.log(`App Listinging on port:  ${port}`)
});