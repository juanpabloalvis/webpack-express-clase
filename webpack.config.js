const path = require("path") // acceder al recurso de nuestra máquina
const nodeExternals = require("webpack-node-externals");

const NODE_ENV = process.env.NODE_ENV;
const PORT = process.env.PORT;

module.exports = {
    name: 'express-server',
    entry: './src/index.ts', // ruta del index.ts
    target: 'node',
    mode: NODE_ENV,
    externals: [nodeExternals()],
    output: { // donde vamos a guardar el recurso
        path: path.resolve(__dirname, 'dist'), // guarda en origen y destino.
        filename: 'index.js' // cómo se llamará la app en prod, puede tener otro nombre
    },
    resolve: {
        extensions: ['.ts', '.js'] //
    },
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader' // usamos el modulo que previamente instalamos
                }
            },
            {
                test: /\.ts$/,
                use: [
                    'ts-loader',
                ]
            }
        ]
    }
}