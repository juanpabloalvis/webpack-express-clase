# webpack-express

npm init -y # permite inicial el proyecto con un package de json

npm install express -S # S de save, indica que la dependencica la vamos a utilizar dentro de nuestro proyecto 

npm install webpack webpack-cli -D # development dependency

npm install @babel/core @babel/preset-env babel-loader # instalar las dependencias de babel para tranpilar y enviarlo a donde necesitemos


ejecutamos la app en \webpack-express-clase>:
node src/index.ts

agregamos los script para start, dev, production.
Ahora podemos ejecutar estos scripts:

 webpack-express-clase> npm run dev


Instalamos typescript en el proyecto:
npm install typescript ts-loader @types/express -D

renombramos el index de .js a .ts

npm install --save-dev tslint

agregamos webpack-externals:
npm install webpack-node-externals -D

